#ifndef NGX_HTTP_HEADER_HASH_H_
#define NGX_HTTP_HEADER_HASH_H_

#include <ngx_config.h>
#include <ngx_core.h>
#include <ngx_http.h>

#define SIZE 32 

/*ngx_module_t ngx_http_header_hash_modulea */;

/*
 * Configuration setup function
 */
char* ngx_http_header_hash(ngx_conf_t *cf, 
    ngx_command_t *cmd, 
    void *conf);

/*
 * Header hash handler function
 */
ngx_int_t ngx_http_header_hash_handler(ngx_http_request_t* request);

#endif
